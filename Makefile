# Name of the project in dropbox/s3
PROJECT_NAME=$(shell cat settings.properties | grep projectName | cut -d "=" -f2 | sed 's/^[ \t]*//')

# Make sure these match the values in app/build.gradle
# android {
#   compileSdkVersion rootProject.compileSdkVersion
#   buildToolsVersion rootProject.buildToolsVersion
# }
ANDROID_VERSION=$(shell cat settings.properties | grep androidVersion | cut -d "=" -f2 | sed 's/^[ \t]*//')
BUILD_TOOLS_VERSION=$(shell cat settings.properties | grep buildToolsVersion | cut -d "=" -f2 | sed 's/^[ \t]*//')

ALPHA_GRADLE_TASK=$(shell cat settings.properties | grep alphaGradleTask | cut -d "=" -f2 | sed 's/^[ \t]*//')
BETA_GRADLE_TASK=$(shell cat settings.properties | grep betaGradleTask | cut -d "=" -f2 | sed 's/^[ \t]*//')
RELEASE_GRADLE_TASK=$(shell cat settings.properties | grep releaseGradleTask | cut -d "=" -f2 | sed 's/^[ \t]*//')

ALPHA_APK_PATH=$(shell cat settings.properties | grep alphaApkPath | cut -d "=" -f2 | sed 's/^[ \t]*//')
BETA_APK_PATH=$(shell cat settings.properties | grep betaApkPath | cut -d "=" -f2 | sed 's/^[ \t]*//')
RELEASE_APK_PATH=$(shell cat settings.properties | grep releaseApkPath | cut -d "=" -f2 | sed 's/^[ \t]*//')

jdk:
	add-apt-repository -y ppa:openjdk-r/ppa
	apt-get update -qq
	apt-get install -y openjdk-8-jdk
	update-java-alternatives -s /usr/lib/jvm/java-1.8.0-openjdk-amd64

android-sdk:
	@echo y | android update sdk --no-ui --all --filter android-$(ANDROID_VERSION) | grep 'package installed'
	@echo y | android update sdk --no-ui --all --filter build-tools-$(BUILD_TOOLS_VERSION) | grep 'package installed'
	@echo y | android update sdk --no-ui --all --filter extra-android-m2repository | grep 'package installed'
	@echo y | android update sdk --no-ui --all --filter extra-google-m2repository | grep 'package installed'

install: jdk android-sdk

init:
	@ls -la $(PROJECT_NAME)
	@cp -rv ./$(PROJECT_NAME)/* ./
	@rm -rf ./$(PROJECT_NAME)/*
	@ls -la

alpha-test: test

beta-test: test

alpha-build: init test
	@sh gradlew $(ALPHA_GRADLE_TASK)
	@ls -la $(APK_PATH)
	@envman add --key BITRISE_APK_PATH --value $(ALPHA_APK_PATH)

beta-build: init test
	@sh gradlew $(BETA_GRADLE_TASK)
	@ls -la $(APK_PATH)
	@envman add --key BITRISE_APK_PATH --value $(BETA_APK_PATH)

release-build: init test
	@sh gradlew $(RELEASE_GRADLE_TASK)
	@ls -la $(APK_PATH)
	@envman add --key BITRISE_APK_PATH --value $(RELEASE_APK_PATH)

publish: release
	@mv ./keys.json ./app/keys.json
	@sh gradlew publishRelease

test: init clean
	@sh gradlew test

clean:
	@sh gradlew clean
